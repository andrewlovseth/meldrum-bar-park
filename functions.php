<?php

/*
	Enqueue Styles & Scripts
*/

function esa_enqueue_child_styles_and_scripts() {

    $dir = get_stylesheet_directory_uri();
    wp_enqueue_style( 'faa-boston-workshops-styles', $dir . '/meldrum-bar-park.css', '', false );
    wp_enqueue_script('faa-boston-workshops-scripts', $dir . '/js/meldrum-bar-park.js', array(), false, true );

}
add_action( 'wp_enqueue_scripts', 'esa_enqueue_child_styles_and_scripts', 99 );


