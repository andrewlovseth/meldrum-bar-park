<?php

    /*
    
        Template Name: Survey

    */

get_header(); ?>

    <?php get_template_part('templates/survey/intro'); ?>

    <?php get_template_part('templates/survey/survey'); ?>

<?php get_footer(); ?>