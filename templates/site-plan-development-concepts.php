<?php

    /*
    
        Template Name: Site Plan Development Concepts

    */

get_header(); ?>
    
    <?php get_template_part('templates/site-plan-development-concepts/intro'); ?>

    <?php get_template_part('templates/site-plan-development-concepts/concepts'); ?>

<?php get_footer(); ?>