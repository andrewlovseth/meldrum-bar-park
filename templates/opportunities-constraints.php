<?php

    /*
    
        Template Name: Opportunities & Constraints

    */

get_header(); ?>
    
    <?php get_template_part('templates/opportunities-constraints/opportunities'); ?>

    <?php get_template_part('templates/opportunities-constraints/constraints'); ?>

    <?php get_template_part('templates/opportunities-constraints/next'); ?>

<?php get_footer(); ?>