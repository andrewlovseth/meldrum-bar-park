<?php

    /*
    
        Template Name: Meldrum Bar Park

    */

get_header(); ?>

    <?php get_template_part('templates/home/hero'); ?>

    <?php get_template_part('templates/home/about-site'); ?>

    <?php get_template_part('templates/home/the-park'); ?>

    <?php get_template_part('templates/home/process'); ?>

    <?php get_template_part('templates/home/timeline'); ?>

    <?php get_template_part('templates/home/get-involved'); ?>

<?php get_footer(); ?>