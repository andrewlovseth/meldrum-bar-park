<?php

    /*
    
        Template Name: Existing Conditions

    */

get_header(); ?>

    <?php get_template_part('templates/existing-conditions/content'); ?>

    <?php get_template_part('templates/existing-conditions/gallery'); ?>

    <?php get_template_part('templates/opportunities-constraints/next'); ?>

<?php get_footer(); ?>